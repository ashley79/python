# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure

import sys
from Tkinter import *
import time
import socket
import threading
from datetime import datetime
import os
import time
import collections
import SearchHistory

path = 'D:\Python\PiRawData.txt'
f = open(path, 'a+')
if os.path.getsize(path) > 100000000 : 
	f = open(path, 'r+')

#show(): drawing data
#info():show info of avg max and min
y = []
x_label = []
customize_port = 0
class UI(Frame):
	def __init__(self, master=None):
		Frame.__init__(self, master)
		self.root = master
		self.label = []
		self.off_show = True
		self.fresh_freq = 10
		self.queueSize = 50
		self.name = ['Duration :', 'Average :', 'Min :', 'Max :']
		self.setup()

	def setup(self):
		f = Figure(dpi=100)
		self.a = f.add_subplot(111)
		self.a.set_xlabel('time(ms)')
		self.a.set_ylabel('RMS')

		canvasContainer = Frame(root, bg = "white", width = 300, height = 300)
		canvasContainer.pack(side=LEFT, expand = 1, fill=BOTH)

		self.canvas = FigureCanvasTkAgg(f, master=canvasContainer)
		self.canvas.show()
		self.canvas.get_tk_widget().pack(side='bottom',fill=BOTH,expand=1)

		self.infoContainer = Frame(root, bg = "white", height=100, width = 100)
		self.infoContainer.pack(side=RIGHT, fill = Y)


		title = Label(master=self.infoContainer,bg='white',text = 'Information ',font='Courier -26 bold')
		title.grid(padx = 0, pady = 20)
		for i in range (0,4) :
			var = StringVar()
			var.set(self.name[i])
			label = Label(master = self.infoContainer, textvariable=var, bg='white',font='Helvetica -18').grid(padx = 6, pady = 8,sticky = 'w')
			self.label.append(var)
			#self.label.append(Label(master = self.infoContainer,bg='white',text = self.name[i],font='Helvetica -18'))
		#	self.label[i].grid(padx = 6, pady = 8,sticky = 'w')
		button_stop = Button(canvasContainer, text='Stop',font='Helvetica -14', command= self.off_showing)
		button_stop.pack(side = LEFT, expand = 1)

		self.variable = StringVar()
		self.variable.set("Resresh Time")
		options=("10", "20", "30", "40", "50")
		op_menu=OptionMenu(canvasContainer, self.variable, *options, command=self.refresh)
		op_menu.pack(side = LEFT, expand = 1)
		op_menu.focus_set()

		self.getQueueSize = StringVar()
		self.getQueueSize.set("Queue Size")
		queue_option=("50", "100", "150", "200")
		queue_menu=OptionMenu(canvasContainer, self.getQueueSize, *queue_option, command=self.adjust_queue)
		queue_menu.pack(side = LEFT, expand = 1)
		queue_menu.focus_set()

		port = Label(canvasContainer,bg='white',text = 'Binding Port')
		port.pack(side = LEFT, expand = 1)
		self.variable1 = StringVar()
		self.variable1.set(port)
		self.txtStart = Entry(canvasContainer)
		self.txtStart.pack(side = LEFT, expand = 1)

		button_conn = Button(canvasContainer, text='Connect',font='Helvetica -14',command= self.bind_port)
		button_conn.pack(side = LEFT, expand = 1)
		button_start = Button(canvasContainer, text='Start',font='Helvetica -14',command= self.on_showing)
		button_start.pack(side = LEFT, expand = 1)

		button_search = Button(master=self.infoContainer, text='Search History', command=self.open,font='Helvetica -14')
		button_search.grid(padx = 10, pady = 15)
		button_quit = Button(master=self.infoContainer, text='Quit', command=self.quit,font='Helvetica -14')
		button_quit.grid(padx = 30, pady = 30)

	def refresh(self, arg):
		try:
			self.fresh_freq = int(arg)
		except Exception as e:
			print("Unexpected error at : refresh", sys.exc_info()[0])
			print(e)

	def adjust_queue(self, arg):
		try:
			self.queueSize = int(arg)
			print self.queueSize
		except Exception as e:
			print("Unexpected error at : adjust_QueueSize", sys.exc_info()[0])
			print(e)

	def bind_port(self):
		global customize_port
		customize_port = int(self.txtStart.get())

	def show(self, i):
		try:
			global y
			global x_label
			while len(y) > self.queueSize :
				y.pop(0)
				x_label.pop(0)
				self.a.set_xlim(x_label[0], x_label[int(self.queueSize)-1])
			if len(y) <= self.queueSize:
				now_x = len(y)
				self.a.set_xlim(x_label[0], x_label[int(now_x)-1])
				self.a.plot(x_label,y,'k')
		except Exception as e:
			print("Unexpected error at : show data()", sys.exc_info()[0])
			print(e)
			pass
		else:
			if self.off_show == False:
				try:
					if i % self.fresh_freq == 0:
						self.canvas.draw()
					if i % 3000 == 0:
						self.a.cla()
				except Exception as e:
					print("Unexpected error at : draw data", sys.exc_info()[0])
					print(e)
	def info(self, infos):
		try:
			infos[0] = str(infos[0])
			for i in range(0, len(self.label)):
				self.label[i].set(self.name[i]+' '+infos[i])
				#self.label[i].config(text = self.name[i]+' '+infos[i])
		except Exception as e:
				print("Unexpected error at :info", sys.exc_info()[0])
				print(infos)
				pass
	def open(self):
		SearchHistory.start_entry()

	def on_showing(self):
		try:
			self.off_show = False
		except Exception as e:
			print(e)
	def  off_showing(self):
		try:
			self.off_show = True
		except Exception as e:
			print(e)

	def quit(self):
		root.quit()
		root.destroy()
		data.quit()

#Receive packet data
#handle_client(): process data
#second(): average second and minutes data
class ReportData():
	def __init__(self):
		self.x = 0
		self.recv_data = dict()
		self.minutes_data = dict()
		self.last_time = ''
		self.time = ''
		self.minima = -1.0
		self.maximum = -1.0
		self.avg = 0
		self.path = 'MACID = 25A'
		self.sum_value = 0
		self.remain_data = ''
		self.pkg_length = 5
		if not os.path.isdir(self.path):
			os.mkdir(self.path)
	def __call__(self):
		self.socketConnect()
	def socketConnect(self):
		bind_ip = "0.0.0.0"
		global customize_port
		while True:
			if customize_port != 0:
				bind_port = customize_port
				self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				self.server.bind((bind_ip, bind_port))
				self.server.listen(5)
				print "[*] Listening on %s:%d" % (bind_ip, bind_port)
				client, addr = self.server.accept()
				print "[*] Acepted connection from: %s:%d" % (addr[0],addr[1])
				self.handle_client(client)
				break
			else:
				time.sleep(1)


	def handle_client(self,client_socket):
		while True:
			try:
				request = client_socket.recv(4096)
				#print "[*] Received: %s" % request
			except Exception as e:
				print("Unexpected error at socket receive", sys.exc_info()[0])
				print(e)
				self.socketConnect()
			else: 
				if request != '':
					slice = request.split()
					for idx, value in enumerate(slice):	
						self.time = datetime.now().strftime("%Y%m%d_%H-%M-%S")
						if self.last_time == '' :
							self.last_time = self.time
						f.write(self.time +' '+ value + str('\n'))

						if len(value.split(',')) < self.pkg_length:
							self.remain_data += value
						if len(self.remain_data.split(',')) == self.pkg_length:
							value = self.remain_data
							self.remain_data = ''

						minus_flag = False

						for pkg in range(len(value.split(','))):
							if value.split(',')[pkg] == '-1.0000':
								minus_flag = True
						if len(value.split(',')) == self.pkg_length and minus_flag == False:
							try:
								if self.recv_data.get(self.time) == None:
									self.recv_data[self.time] = []
								self.recv_data[self.time].append(value)
								total = float(value.split(',')[3])
							except ValueError:
								print("Unexpected error at convert pkcket data to float", sys.exc_info()[0])
								pass
							except Exception as e:
								print(e)
							else:
								try:
									self.sum_value += total
									global y
									y.append(total)
									x_label.append(int(value.split(',')[0]))
									self.x += 1
									if self.minima == -1.0 and self.maximum == -1.0:
										self.minima = self.maximum = total
									if total > self.maximum:
										self.maximum = total
									elif total < self.minima:
										self.minima = total
									self.avg = self.sum_value / self.x
								except Exception as e:
									print 'mix, max value error'
									print(e)
								else:
									info = [self.x, '%.2f' % self.avg,'%.2f' % self.minima,'%.2f' % self.maximum]
									app.info(info)
									info[:] = []
									app.show(self.x)
									self.second(self.time, self.last_time)
				else:
					client_socket.close()
					self.server.close()
					self.socketConnect()
					f.close()
	def second(self,t, last_t):
		SummaryTable = open(self.path + "\\" + "SummaryTable.txt", "a+")

		if t != last_t: #if time changes then calculate 
			try:
				now = t.split('-')
				past = last_t.split('-')
				sum_v = sum_f = sum_p = 0
				data_list = self.recv_data[last_t]
				length = 0
			except Exception as e:
				print 'second Data error'
				print(e)
			else:
				if len(data_list) >= 1: 
					length = len(data_list)
					for index, value in enumerate(data_list):
						slice_data = value.split(',')
						if len(slice_data) == self.pkg_length :
							voltage = float(slice_data[1])
							current = float(slice_data[2])
							power = float(slice_data[3])
							sum_v += voltage
							sum_f += current
							sum_p += power
			#               print 'voltage = %s current = %s power = %s length = %s' %(voltage , current ,power, length) 
						if length > 0 :
							sum_v = (sum_v/length)
							sum_f = (sum_f/length)
							sum_p = (sum_p/length)

					if self.minutes_data.get(past[0]+past[1]) == None:
						self.minutes_data[past[0]+past[1]] = {}
						self.minutes_data[past[0]+past[1]]["v"] = []
						self.minutes_data[past[0]+past[1]]["f"] = []
						self.minutes_data[past[0]+past[1]]["p"] = []
					self.minutes_data[past[0]+past[1]]["v"].append('%.2f' % float(sum_v))
					self.minutes_data[past[0]+past[1]]["f"].append('%.2f' % float(sum_f))
					self.minutes_data[past[0]+past[1]]["p"].append('%.2f' % float(sum_p))

				if not os.path.exists(self.path+'\\'+ past[0] + 'S'+'.txt'): #second first file
					SecondTable = open(self.path+'\\'+ past[0] + 'S'+'.txt', 'a+')
					SummaryTable.write('\n' + past[0] + ',' + past[0]+'S'+'.txt'+ ',')
				else:
					SecondTable = open(self.path+'\\'+ past[0] + 'S'+'.txt','a+')

				SecondTable.write(self.last_time+','+ str('%.2f'%sum_v)+','+str('%.2f'%sum_f)+','+str('%.2f'%sum_p)+str('\n'))

				if now[1] != past[1]: # pass 1 minutes
					if not os.path.exists(self.path+'\\'+ past[0] +'.txt'): #minutes first file
						MinutesTable = open(self.path+'\\'+ past[0] +'.txt','a+')
						SummaryTable.write(past[0] +'.txt')
					else:
						MinutesTable = open(self.path+'\\'+past[0] + '.txt','a+')
					v = f = p = 0
					if past[0]+past[1] in self.minutes_data:
						minData = self.minutes_data[past[0]+past[1]]
						if minData["v"].count > 0 and minData["f"].count > 0 and minData["p"].count > 0:
							for idx in range (len(minData["v"])):
								v += float(minData["v"][idx])
								f += float(minData["f"][idx])
								p += float(minData["p"][idx])
							v = v/len(minData["v"])
							f = f/len(minData["f"])
							p = p/len(minData["p"])
							print 'average v = %s ct = %s p = %s' %(v, f, p)
							MinutesTable.write(past[0] + '-'+past[1] +','+ str('%.2f' % v) + ',' +str('%.2f' % f) + ',' +str('%.2f' % p) + str('\n'))
						del self.minutes_data[past[0]+past[1]]
						MinutesTable.close()

				self.last_time = self.time
				if self.minutes_data.has_key(str([past[0]+past[1]])):
					del self.minutes_data[past[0]+past[1]]
				del self.recv_data[last_t]
				SecondTable.close()
				SummaryTable.close()
	def quit(self):
		os._exit(0)

if __name__ == '__main__':
	root = Tk()
	root.wm_title("Receive from Raspberry")
	app = UI(master=root)
	data = ReportData()
	t = threading.Thread(target=data, name='bg')
	t.start()
	app.mainloop()
