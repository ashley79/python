# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use('TkAgg')

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
from Tkinter import *
import sys
from datetime import datetime
import tkMessageBox
import ttkcalendar
import tkSimpleDialog
import collections
import time
import os

class CalendarDialog(tkSimpleDialog.Dialog):
    def body(self, master):
        self.calendar = ttkcalendar.Calendar(master)
        self.calendar.pack()

    def apply(self):
        if self.calendar.selection is None:
            value = '%s' %time.strftime('%Y%m%d')
            self.result = value
        else:
            shift = self.calendar.selection.strftime("%Y%m%d")
            value = '%s' %(shift)
            self.result = value
            
class Main(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.root = master
        self.end_sup = '_9'
        self.flag = False
        self.flag1 = False
        self.path = 'MACID = 25A'
        self.name = []
        self.y = []
        self.h_unit = []
        self.color_set = []
        self.SummaryTable = open(self.path + "\\" + "SummaryTable.txt", "r")
        self.setup()

    def setup(self):
        f = Figure(figsize=(7, 5), dpi=100)
        self.a = f.add_subplot(111)
        self.s_canvas = FigureCanvasTkAgg(f, master=self.root)
        self.s_canvas.show()
        self.s_canvas._tkcanvas.pack(side=BOTTOM, fill=BOTH, expand=1)
        self.file_list()
        self.variable = StringVar(self.master)
        options=("Multiday", "Day", "Hour")
        op=OptionMenu(self.master, self.variable, *options, command=self.select_time_unit)
        op.pack(side = LEFT, expand = 1)
        self.variable.set("Time Unit")
        op.focus_set()

        self.start_date = StringVar(self.master)
        self.button_date = Button(self.master, text='Select Begin Date', command=lambda: self.selectDate(self.start_date))
        self.button_date.pack(side = LEFT)
        self.end_date = StringVar(self.master)

        button_quit = Button(self.master, text='Quit', command=self.quit)
        button_quit.pack(side = RIGHT)
        button_set = Button(self.master, text='Set', command=self.setting)
        button_set.pack(side = RIGHT, expand = 1)

    def selectDate(self,obj):
        cd = CalendarDialog(self)
        obj.set(cd.result)
        self.button_date.configure(textvariable=self.start_date)
    def selectEndDate(self,obj):
        ed = CalendarDialog(self)
        obj.set(ed.result)
        self.button_date1.configure(textvariable=self.end_date)

    def quit(self):
        self.root.quit()
        self.root.destroy()

    def select_time_unit(self,arg):
        if self.variable.get() == 'Hour' and self.flag == False:
            self.h_unit[:] = []
            self.hour = StringVar(self.master)
            for i in range (0,24) :
                self.h_unit.append(i)
            self.ho=OptionMenu(self.master, self.hour, *self.h_unit, command=self.select_time_unit)
            self.ho.pack(side = LEFT, expand = 1)
            self.hour.set("Select Hour")
            self.ho.focus_set()
            self.flag = True
        elif self.flag == True and self.variable.get() != 'Hour':
            self.ho.destroy()
            self.flag = False
        if self.variable.get() != 'Multiday' and self.flag1 == True:
            self.button_date1.destroy()
            self.flag1 = False
        elif self.variable.get() == 'Multiday' and self.flag1 == False :
            self.button_date1 = Button(self.master, text='Select End Date', command=lambda: self.selectEndDate(self.end_date))
            self.button_date1.pack(side = LEFT)
            self.flag1 = True

    def file_list(self):
        self.file_list = []
        for lines in self.SummaryTable.readlines():
            if lines != '\n':
                self.file_list.append(lines.split(',')[0])
        self.begin = self.file_list[0]
        self.end = self.file_list[-1]
        self.label = Label(master=self.root, text='You can choose from %.8s to %.8s'% (self.begin, self.end), font=('Courier New', -16, 'bold'))
        self.label.pack(side = TOP)

    def setting(self):
        self.y[:] = []
        self.name[:] = []
        self.a.cla()
        Flag = False
        if self.variable.get() == 'Hour' :
            HH = self.hour.get()
            hour_state = str(self.start_date.get())
            if int(HH) < 10:
                HH = '0' + HH
            date = hour_state+'_'+HH
            for idx,value in enumerate(self.file_list):
                if date == self.file_list[idx]:
                    Flag = True

            if Flag == True:
                with open(self.path + "\\" + date+'.txt', 'r') as file:
                    for line in file:
                        if line.split(',')[3] != '0\n':
                            hour = line.split(',')[0]
                            if hour[12] == '0':
                                self.name.append(hour[13:14])
                            else:
                                self.name.append(hour[12:14])
                            self.y.append(line.split(',')[3].rstrip('\n'))
                    self.bar_setting(None,0)
            else:
                self.error('hour file doesn''t exists')

        elif self.variable.get() == 'Day' or self.variable.get() == 'Multiday' :
            day_avg = {}
            if self.variable.get() == 'Multiday' and self.start_date.get()<self.end_date.get():
                hour_state = str(self.start_date.get())
                for idx, value in enumerate(self.file_list):
                    if self.start_date.get()<= self.file_list[idx] and self.end_date.get()+self.end_sup >= self.file_list[idx]:
                        try:
                            with open(self.path + "\\" + self.file_list[idx]+'.txt', 'r') as file:
                                for line in file:
                                    hour = line.split('_')[0] # day
                                    if day_avg.get(hour) == None:
                                        day_avg[hour] = []
                                    if line.split(',')[3] != '0\n':
                                        day_avg[hour].append(line.split(',')[3].rstrip('\n'))
                        except:
                            pass
                self.bar_setting(day_avg, 1)
            
            if self.variable.get() == 'Day':
                hour_state = str(self.start_date.get())
                for idx, value in enumerate(self.file_list):
                    if self.start_date.get() == self.file_list[idx][0:8]:
                        try:
                            with open(self.path + "\\" + self.file_list[idx]+'.txt', 'r') as file:
                                for line in file:
                                    hour = line.split('-')[0].split('_')[1] # hour
                                    if day_avg.get(hour) == None:
                                        day_avg[hour] = []
                                    if line.split(',')[3] != '0\n':
                                        day_avg[hour].append(line.split(',')[3].rstrip('\n'))
                        except:
                            pass
                self.bar_setting(day_avg, 2)
        else:
            self.error('invalid input/output date')

    def bar_setting(self, data, unit):
        t_unit = ['Minutes','Day', 'Hour',]
        sum = 0.0
        self.color_set[:] = []
        if unit != 0:
            order = collections.OrderedDict(sorted(data.items()))
            for (k, v) in order.iteritems():
                for idx in v:
                    sum += float(idx)
                sum = sum/len(v)
                self.name.append(k)
                self.y.append(sum)
            for i, v in enumerate(self.y):
                self.group_color(v)
                self.a.text(i, v+0.2, '%.1f' % float(v), ha='center',color='#1f77b4', fontweight='bold')
        else:
             for i, v in enumerate(self.y):
                self.group_color(v)
                self.a.text(i, float(v)+0.3, '%.1f' % float(v), ha='center', color='brown', fontweight='bold', size = 9, rotation= 23)
        self.a.set_xlabel(t_unit[unit])
        self.a.set_ylabel('Average RMS')
        self.a.bar(range(len(self.y)), self.y, 0.8,color = self.color_set, tick_label=self.name)
        self.a.set_xbound(-1,len(self.y))
        self.a.grid(True)
        self.s_canvas.show()

    def group_color(self, arg):
        arg = '%.0f' % float(arg)
        arg = int(arg)
        if arg<100.0: self.color_set.append('hotpink')
        elif arg>=100 and arg < 200: self.color_set.append('springgreen')
        elif arg>=200 and arg < 300: self.color_set.append('skyblue')
        elif arg>=300 and arg < 400: self.color_set.append('lightgray')
        elif arg>=400: self.color_set.append('goldenrod')

    def error(self, word):
        tkMessageBox.showinfo('Warning',word)

def start_entry():
    root = Tk()
    root.wm_title("Embedding in TK")
    app = Main(master = root)
    app.mainloop()

if __name__ == '__main__':
    start_entry()