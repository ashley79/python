# -*- coding: utf-8 -*-
import hashlib
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy.orm import sessionmaker
import sqlalchemy
from datetime import datetime

Base = declarative_base()

class Data(Base):
    __tablename__ = 'data'
    time = Column(String,primary_key = True)
    voltage = Column(Float)
    current = Column(Float)
    power = Column(Float)

class tag(Base):
    __tablename__ = 'tag'
    processed_id = Column(String, primary_key = True)
    tag = Column(sqlalchemy.Integer)

class hourAvg(Base):
    __tablename__ = 'houravg'
    hour = Column(String, primary_key = True)
    v_avg = Column(sqlalchemy.Float)
    c_avg = Column(sqlalchemy.Float)
    p_avg = Column(sqlalchemy.Float)

if __name__ == '__main__':
    engine = create_engine('sqlite:///SQL.db', echo=True)
    Base.metadata.create_all(engine)